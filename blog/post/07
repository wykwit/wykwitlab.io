AUR
2018-04-30


AUR - Arch User Repository
Something every Arch Linux user MUST be familiar with.
In this post, I'll describe how to install yaourt (pacman wrapper with AUR support) from AUR. That way you'll know how to work with AUR and you'll have a tool which will relieve you from ever doing that AUR stuff manually again.

https://aur.archlinux.org/packages/yaourt/

Update: yaourt is not the coolest kid in town anymore. Check out yay instead.
https://aur.archlinux.org/packages/yay/

First of all: dependencies. Dependencies are packages needed for a certain package to work. We need to resolve them manually, installing them before yaourt. They're always listed on the packages page. To install packages, marking them as dependencies we'll use:
$ pacman -S --asdeps [packagename]
You may have some of package's dependencies already installed on your system. To skip reinstallation process use '--needed' flag. So for our yaourt we would issue a following command:
# pacman -S --needed --asdeps diffutils gettext pacman rsync
Note that one of yaourt's dependencies, namely package-query, is not in the official repositories and that's why I didn't include it in the command above. It is (also) available in the AUR!
We have to install package-query before we can proceed. Fortunately all it's dependecies are in the repos.
Now we'll install our first package from AUR - package-query!

https://aur.archlinux.org/packages/package-query/

For that we'll need a PKGBUILD. It is a file describing build and installation process of which proper programs then take care.
Historically AUR was just a database for those PKGBUILDs. Now it's a little bit more complex; links we'd need are in the upper right corner of the AUR package page.
$ curl https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=package-query -o PKGBUILD
$ makepkg -si
makepkg will make a package, -s flag will (once again) check for dependencies and resolve them with pacman, and -i flag will install the package (with pacman) when it's ready. As you issue these commands the package should be installed.
Now you can repeat the process for yaourt, safely omitting -s flag; we've already taken care of dependencies.
$ curl https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=yaourt -o PKGBUILD
$ makepkg -i

If everything went right you'll have working yaourt installed now. From the user perspective it works just like pacman, but includes AUR. Now you can install AUR packages with just a:
$ yaourt -S [packagename]
Search through AUR and repositories with:
$ yaourt -Ss [search term]
And update everything, including AUR packages with just a:
$ yaourt -Syua
Now, when you query for orphans (unused packages):
$ yaourt -Qdt
yaourt will ask you if you want to remove them, which of course you do. Pretty convenient.
You can work with yaourt like you would with pacman (it's just a wrapper). For instance, to remove something:
$ yaourt -Rncsuv [packagename]
This will give exactly the same result as:
$ pacman -Rncsuv [packagename]

Hope you like your new toys. Have fun!
