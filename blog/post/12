Libreboot
2019-06-13


A quick libreboot cheat-note, useful when you want to update (even if it's just your background or config).
Use tools from the libreboot package: <a href="https://libreboot.org/">libreboot.org</a>
my version is: r20160907


For flashing boot with kernel parameter:
&emsp;iomem=relaxed

Verify flashrom (it'll tell you whether it's working properly or not):
$ flashrom -p internal -V

Dump current rom:
$ flashrom -p internal -r [path/to/file]

Read rom content:
$ cbfstool [romfile] print

Extract file from rom:
$ cbfstool [romfile] extract -n [filename] -f [filename]

Replace a file in rom:
$ cbfstool remove -n [filename]
$ cbfstool add -n [filename] -f [path/to/file] -t raw

Flash updated rom:
$ flash update [path/to/rom]


That's about it. If you don't have libreboot installed yet, see the installation guide and documentation first: <a href="https://libreboot.org/docs/">https://libreboot.org/docs/</a>
Before you install, make sure your device is on the list of supported hardware: <a href="https://libreboot.org/docs/hardware">https://libreboot.org/docs/hardware/</a>

If you're not sure why you'd want to run libreboot, here's a quote from the project's website:
"Many people use non-free proprietary boot firmware, even if they use GNU+Linux. Non-free BIOS/UEFI firmware often contains backdoors, can be slow and have severe bugs. Development and support can be abandoned at any time. By contrast, libreboot is fully free software, where anyone can contribute or inspect its code. [...] Libreboot is faster, more secure and more reliable than most non-free firmware. Libreboot provides many advanced features, like encrypted /boot/, GPG signature checking before booting a Linux kernel and more!"
