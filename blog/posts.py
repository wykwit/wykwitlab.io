import json
import os

path = "post/"
output_json = "posts.json"
output_html = path+"index.html"

output = []
for post in os.listdir(path):
	if "index.html" in post: continue
	with open(path+post, "r") as f:
		output.append({
			"id": post,
			"title": f.readline().strip(),
			"date": f.readline().strip()
		})
output = sorted(output, key=lambda x: x["date"], reverse=True)

with open(output_json, "w") as f:
	json.dump(output, f)

with open(output_html, "w") as f:
	f.write("<html><body>"+"".join(["<li><a href='{id}'>{id} - {t}</a></li>".format(id=x["id"], t=x["title"]) for x in output])+"</body></html>")

