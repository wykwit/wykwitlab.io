function download(target, callback) {
	request = new XMLHttpRequest();
	request.onreadystatechange = function() {
		if(this.readyState == 4 && this.status == 200)
			callback(this.responseText);
	};
	request.open("GET", target, true);
	request.send();
}

var load_list = _ => download("posts.json", function(data) {
	var output = "";
	data = JSON.parse(data);
	for(var i = 0; i < data.length; i++) {
		var post_id = (data.length-i <= 9) ? "0" + String(data.length-i) : String(data.length-i);
		output += "<a href=\"#post-" + post_id + "\"><h3>"
			+ data[i].title + "</h3><h4>" + data[i].date + "</h4></a>\n";
	}
	document.getElementById("content").innerHTML = output;
});

function load_content() {
	var skip_tags_open = ["<pre>", "<ol>", "<ul>"];
	var skip_tags_close = ["</pre>", "</ol>", "</ul>"];

	var link_target = location.hash.split("-");
	post_location = "post/" + link_target.pop();
	if(link_target.pop() == "#post") {
		download(post_location, function(data) {
			var output = "";
			data = data.split("\n");

			output += "<h3>" + data.shift() + "</h3>\n";
			output += "<h4>" + data.shift() + "</h4><br>\n";
			while(data[0] == "") data.shift();

			var skip_tag = false
			for(var i = 0; i < data.length; i++) {
				if(skip_tags_open.indexOf(data[i]) != -1) {
					skip_tag = true;
					data[i] = "<br>" + data[i];
				} else if(skip_tags_close.indexOf(data[i]) != -1) {
					skip_tag = false;
				} else if(!skip_tag) {
					data[i] += "<br>";
				}
			}
			output += "<article>\n" + data.join("\n") + "</article>\n";
			document.getElementById("content").innerHTML = output;
		});
	} else {
		load_list();
	}
}

window.onhashchange = load_content;
load_content();
