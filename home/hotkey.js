var options = document.getElementsByTagName("li");
var labels = ["z", "x", "c", "v", "b", "n", "m"];

function click_option(e) {
	var ch = labels.indexOf(e.key);
	var link = options[ch].children[0];
	if(!link || !link.href) return;
	try { window.location.href = link.href; } catch(err) {}
}

document.body.onkeypress = e => click_option(e);
for(var i = 0; i < options.length; i++)
	options[i].innerHTML += "<br><i>"+labels[i]+"</i>";

